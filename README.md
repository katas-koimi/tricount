## Tricount

Inspired by the [Tricount](https://www.tricount.com/) application, 
I'm doing this project to reproduce how it works by applying 
TDD principles.

### Principle
The principle of the application is to allow a group of people 
to add expenses to a common event, and ultimately 
to balance these expenses between everyone.